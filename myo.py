from __future__ import print_function
from collections import Counter, deque
import sys
import numpy as np
#import pigpio
try:
    from sklearn import neighbors, svm, ensemble
    HAVE_SK = True
except ImportError:
    HAVE_SK = False
HAVE_SK = True
from common import *
from myo_raw import MyoRaw
#import RPi.GPIO as GPIO
import time
SUBSAMPLE = 20
K = 25
class NNClassifier(object):
    '''A wrapper for sklearn's nearest-neighbor classifier that stores
    training data in vals0, ..., vals9.dat.'''
    def __init__(self):
        for i in range(10):
            with open('vals%d.dat' % i, 'ab') as f: pass
        self.read_data()

    def store_data(self, cls, vals):
        with open('vals%d.dat' % cls, 'ab') as f:
            f.write(pack('8H', *vals))

        self.train(np.vstack([self.X, vals]), np.hstack([self.Y, [cls]]))

    def read_data(self):
        X = []
        Y = []
        for i in range(10):
            X.append(np.fromfile('vals%d.dat' % i, dtype=np.uint16).reshape((-1, 8)))
            Y.append(i + np.zeros(X[-1].shape[0]))
        self.train(np.vstack(X), np.hstack(Y))

    def train(self, X, Y):
        self.X = X
        self.Y = Y
        if HAVE_SK and self.X.shape[0] >= K * SUBSAMPLE:
            #self.nn = neighbors.KNeighborsClassifier(n_neighbors=K, algorithm='kd_tree')
            #self.nn = svm.SVC(kernel='poly',probability=True,gamma= 2)
            self.nn = ensemble.GradientBoostingClassifier(n_estimators=K)
            self.nn.fit(self.X[::SUBSAMPLE], self.Y[::SUBSAMPLE])
        else:
            self.nn = None

    def nearest(self, d):
        dists = ((self.X - d)**2).sum(1)
        ind = dists.argmin()
        return self.Y[ind]

    def classify(self, d):
        if self.X.shape[0] < K * SUBSAMPLE: return 0
        if not HAVE_SK: return self.nearest(d)
        return self.nn.predict([d])[0]


class Myo(MyoRaw):
    '''Adds higher-level pose classification and handling onto MyoRaw.'''
    HIST_LEN = 25
    def __init__(self, cls, tty=None):
        MyoRaw.__init__(self, tty)
        self.cls = cls
        self.history = deque([0] * Myo.HIST_LEN, Myo.HIST_LEN)
        self.history_cnt = Counter(self.history)
        self.add_emg_handler(self.emg_handler)
        self.last_pose = None
        self.pose_handlers = []
        '''Init Servo'''
        '''GPIO.setmode(GPIO.BCM)
        GPIO.setup(02, GPIO.OUT)
        GPIO.setup(03, GPIO.OUT)
        GPIO.setup(04, GPIO.OUT)
        self.p02 = GPIO.PWM(02, 50)
        self.p02.start(0)
        self.p03 = GPIO.PWM(03, 50)
        self.p03.start(0)
        self.p04 = GPIO.PWM(04, 50)
        self.p04.start(0)'''


    def move_servo_02(self,value):
        self.p02.ChangeDutyCycle(value)
    def move_servo_03(self,value):
        self.p03.ChangeDutyCycle(value)
    def move_servo_04(self,value):
        self.p04.ChangeDutyCycle(value)


    def emg_handler(self, emg, moving):
        y = self.cls.classify(emg)
        if not HAVE_SK:
            self.history_cnt[self.history[0]] -= 1
            self.history_cnt[y] += 1
        self.history.append(y)
        print(str(y))
        if str(y) == '0.0':
            print (str(y))
            #self.move_servo_02(12.5)
            #self.move_servo_03(12.5)
            #self.move_servo_04(12.5)

        if str(y) == '1.0':
            print(str(y))
            #self.move_servo_02(2.5)
            #self.move_servo_03(12.5)
            #self.move_servo_04(12.5)

        if str(y) == '2.0':
            print(str(y))
            #self.move_servo_02(2.5)
            #self.move_servo_03(2.5)
            #self.move_servo_04(12.5)

        if str(y) == '3.0':
            print(str(y))
            #self.move_servo_02( 2.5)
            #self.move_servo_03( 2.5)
            #self.move_servo_04( 2.5)

        r, n = self.history_cnt.most_common(1)[0]
        if self.last_pose is None or (n > self.history_cnt[self.last_pose] + 5 and n > Myo.HIST_LEN / 2):
            self.on_raw_pose(r)
            self.last_pose = r

    def add_raw_pose_handler(self, h):
        self.pose_handlers.append(h)

    def on_raw_pose(self, pose):
        for h in self.pose_handlers:
            h(pose)

if __name__ == '__main__':

    m = Myo(NNClassifier(), sys.argv[1] if len(sys.argv) >= 2 else None)
    '''m.add_raw_pose_handler(print)
    def page(pose):
        if pose == 5:
            print (pose)
        elif pose == 6:
            print(pose)
    m.add_raw_pose_handler(page)'''
    m.connect()
    while True:
        m.run()